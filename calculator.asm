.data

str0: .asciiz "--------Welcome to Mips Calculator-------- \n"
str1: .asciiz "Please Enter how many Operations you want: \n"
str2: .asciiz "\nPlease Enter an Operand ( A, S, D, M, R, F) : \n"
str3: .asciiz "\nEnter Number 1: \n"
str4: .asciiz "\nEnter Number 2: \n"
str5: .asciiz "\n---Addition---\n"
str6: .asciiz "\n---Subtraction---\n"
str7: .asciiz "\n---Division---\n"
str8: .asciiz "\n---Multiplication---\n"
str9: .asciiz "\n---Remainder---\n"
str10: .asciiz "\n---Factorial---\n"
str11: .asciiz "\n---Select Operation ( P, R, Q )---\n"
str12: .asciiz "\n---Enter a number or A---\n"
str13: .asciiz "answer "
str14: .asciiz " : "
str15: .asciiz "\n-----------\n"
str16: .asciiz "\n------Hope You Love this Calculator------\n------The End Bye :)) !!------\n"

.text
.globl main

main:
	move $fp, $sp

	add $s0, $sp, 0;

	li $v0, 4
	la $a0, str0
	syscall

	li $v0, 4
	la $a0, str1
	syscall

	li $v0, 5
	syscall
	move $t0, $v0
	sub $t0, $t0, 1

	loop:
	    bgt $t1, $t0, Print

	 	li $v0, 4
		la $a0, str2
		syscall

		li $v0, 12
		syscall
		move $t2, $v0

		beq $t2, 0x41, Addition # A
		beq $t2, 0x53, Subtraction # S
		beq $t2, 0x44, Division # D
		beq $t2, 0x4d, Multiplication # M
		beq $t2, 0x52, Remainder # R
		beq $t2, 0x46, Factorial # F 

	    addi $t1, $t1, 1
	    j loop 

Addition:
	li $v0, 4
	la $a0, str5
	syscall

	li $v0, 4
	la $a0, str3
	syscall

	li $v0,5
	syscall
	move $t3, $v0

	li $v0, 4
	la $a0, str4
	syscall

	li $v0,5
	syscall
	move $t4, $v0

	add $t5, $t4, $t3

	sw $t5, 0($s0)
	add $s0, $s0, 4

	addi $t1,$t1,1
	j loop 

Subtraction:
	li $v0, 4
	la $a0, str6
	syscall

	li $v0, 4
	la $a0, str3
	syscall

	li $v0,5
	syscall
	move $t3, $v0

	li $v0, 4
	la $a0, str4
	syscall

	li $v0,5
	syscall
	move $t4, $v0

	sub $t5, $t3, $t4

	sw $t5, 0($s0)
	add $s0, $s0, 4

	addi $t1,$t1,1
	j loop

Division:
	li $v0, 4
	la $a0, str7
	syscall

	li $v0, 4
	la $a0, str3
	syscall

	li $v0,5
	syscall
	move $t3, $v0

	li $v0, 4
	la $a0, str4
	syscall

	li $v0,5
	syscall
	move $t4, $v0

	div $t5, $t3, $t4

	sw $t5, 0($s0)
	add $s0, $s0, 4

	addi $t1,$t1,1
	j loop 

Multiplication:
	li $v0, 4
	la $a0, str8
	syscall

	li $v0, 4
	la $a0, str3
	syscall

	li $v0,5
	syscall
	move $t3, $v0

	li $v0, 4
	la $a0, str4
	syscall

	li $v0,5
	syscall
	move $t4, $v0

	mul $t5, $t3, $t4

	sw $t5, 0($s0)
	add $s0, $s0, 4

	addi $t1,$t1,1
	j loop

Remainder:
	li $v0, 4
	la $a0, str9
	syscall

	li $v0, 4
	la $a0, str3
	syscall

	li $v0,5
	syscall
	move $t3, $v0

	li $v0, 4
	la $a0, str4
	syscall

	li $v0,5
	syscall
	move $t4, $v0

	rem $t5, $t3, $t4

	sw $t5, 0($s0)
	add $s0, $s0, 4

	addi $t1,$t1,1
	j loop

Factorial:
	li $v0, 4
	la $a0, str10
	syscall

	li $v0, 4
	la $a0, str3
	syscall

	li $v0,5
	syscall
	move $t3, $v0

	li $t4,1 
	 fac: mul $t4, $t4, $t3 
	 addi $t3, $t3, -1 
	 bgtz $t3, fac 		

	sw $t4, 0($s0)
	add $s0, $s0, 4

	addi $t1,$t1,1
	j loop

Print:
	li $v0, 4
	la $a0, str11
	syscall

	li $v0, 12
	syscall
	move $t0, $v0

	add $s1, $sp, 0;

	beq $t0, 0x50, Ppart
	beq $t0, 0x52, Reset
	beq $t0, 0x51, exit
	bne $t0, 0x51, Print

Ppart:
	li $v0, 4
	la $a0, str12
	syscall

	li $v0, 5
	syscall
	move $s4, $v0

	add $s1, $sp, 0;

	bne $v0, 0x00, PNUM
	beq $v0, 0x00, PALL

	PNUM:
		li $v0, 4
		la $a0, str15
		syscall

		li $v0, 4
		la $a0, str13
		syscall

		move $t1, $s4

		li $v0, 1
		move $a0, $t1
		syscall

		li $v0, 4
		la $a0, str14
		syscall

		add $t1, $t1, -1
		mul $t1, $t1, 4
		add $t1, $t1, $sp
		
		lw $a0, 0($t1)
		li $v0, 1
		syscall

		li $v0, 4
		la $a0, str15
		syscall

		j Print

	PALL:
		li $v0, 4
		la $a0, str13
		syscall

		sub $s2, $s1, $sp
		div $s2, $s2, 4
		add $s2, $s2, 1

		li $v0, 1
		move $a0, $s2
		syscall

		li $v0, 4
		la $a0, str14
		syscall

		lw $a0, 0($s1)
		li $v0, 1
		syscall

		li $v0, 4
		la $a0, str15
		syscall

		add $s1, $s1, 4
		bne $s0, $s1, PALL	
j Print


Reset:
	li $s3, 0
	sw $s3, 0($s1)
	add $s1, $s1, 4
	bne $s0, $s1, Reset
j Print

exit:
	li $v0, 4
	la $a0, str16
	syscall

	li $v0, 10
	syscall